package me.changani.sorting;

public class SelectionSort {
	public static void main(String[] args) {
		int[] list = { 20, 35, -15, 7, 55, 1, -22 };

		for(int lastUnsortedIndex = list.length - 1; lastUnsortedIndex > 0; lastUnsortedIndex--) {
			int largest = 0;
			
			for(int i = 1; i <= lastUnsortedIndex; i++) {
				if (list[i] > list[largest]) {
					largest = i;
				}
			}
			
			swap(list, largest, lastUnsortedIndex);
		}
		

		printList(list);
	}

	public static void printList(int[] list) { 
		for (int j = 0; j < list.length; j++) {
			System.out.println(list[j]);
		}
	}

	public static void swap(int[] list, int i, int j) {
		if (i == j) {
			return;
		}

		// Save temporary
		int temp = list[i];

		// Swap
		list[i] = list[j];
		list[j] = temp;
	}
}
