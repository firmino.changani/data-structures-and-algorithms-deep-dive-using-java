package me.changani.sorting;

public class SelectionSortSecond {
	
	public static void main(String[] args) {
		int[] list = { 20, 35, -15, 7, 55, 1, -22 };
		
		printList(sort(list));
	}
	
	public static int[] sort(int[] list) {
		
		for(int lastUnsortedIndex = list.length - 1;  lastUnsortedIndex > 0; lastUnsortedIndex--) {
			int largestIndex = 0;
			for (int i = 1; i <= lastUnsortedIndex; i++) {
				if (list[i] > list[largestIndex]) {
					largestIndex = i;
				}
			}
			
			swap(list, largestIndex, lastUnsortedIndex);
		}
		
		return list;
	}
	
	
	public static void printList(int[] list) {
		for(int i = 0; i < list.length; i++) {
			System.out.println(list[i]);
		}
	}
	
	public static void swap(int[] list, int i, int j) {
		if (i == j) {
			return;
		}
		
		// Hold i value temporarily
		int temp = list[i];
		
		// Swap values
		list[i] = list[j];
		list[j] = temp;
	}
}
