package me.changani.sorting;

public class InsertionSort {
	public static void main(String[] args) {
		int[] list = { 20, 35, -15, 7, 55, 1, -22 };
		
		
		for(int firstUnsortedIndex = 1; firstUnsortedIndex < list.length; firstUnsortedIndex++) {
			int newElement = list[firstUnsortedIndex];
			int i;
			
			for (i = firstUnsortedIndex; i > 0 && list[i - 1] > newElement; i--) {
				list[i] = list[i - 1]; 
			}
			
			list[i] = newElement;
		}
		
		printList(list);
	}
	
	public static void printList(int[] list) { 
		for (int j = 0; j < list.length; j++) {
			System.out.println(list[j]);
		}
	}
}
