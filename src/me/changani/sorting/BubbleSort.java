package me.changani.sorting;


/*
 * Worst-time complexity: O(n^2) Quadratic
 */
public class BubbleSort {
	public static void main(String[] args) {
		int[] intArray = { 20, 35, -15, 7, 55, 1, -22 };

		// Iterate over the last unsorted index
		for (int lastUnsortedIndex = intArray.length - 1; lastUnsortedIndex > 0; lastUnsortedIndex--) {
			// Iterate the unsorted values
			for(int i = 0; i < lastUnsortedIndex; i++) {
				// Check whether values current and next should be swapped
				if (intArray[i] > intArray[i + 1]) {
					// Array, current and next value
					swap(intArray, i, i + 1);
				}
			}
		}
		
		
		for(int k = 0; k < intArray.length; k++) {			
			System.out.println(intArray[k]);
		}
	}

	public static void swap(int[] array, int i, int j) {
		if (i == j) {
			return;
		}

		// Temporary
		int temp = array[i];

		// Swap both values
		array[i] = array[j];
		array[j] = temp;
	}
}
